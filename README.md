# Ideaware React

I initialize this project using my own react-boilerplate which includes a custom webpack and babel configuration, you may find this project here https://github.com/Jnlbr/react-boilerplate (is a working progress)

## Custom commands

To create a component:

`yarn generate component <NAME>`

To create a container component:

`yarn generate container <NAME>`

## Development server

Run `yarn start` to start de dev server.

## Run tests

Run `yarn jest` to run all tests, to run a single test use `yarn jest -t 'TEST_NAME'`
