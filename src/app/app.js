// @flow
import React from 'react';
// Styles
import './app.scss';
// Router
import Routes from './routes';

type Props = {};

const App = (props: Props) => {
	return (
		<div>
			<Routes />
		</div>
	);
};

export default App;
