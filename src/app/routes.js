import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
// Pages
import Home from './containers/home';

const Routes = () => (
	<BrowserRouter>
		<Switch>
			<Route path="/">
				<Home />
			</Route>
		</Switch>
	</BrowserRouter>
);

export default Routes;
