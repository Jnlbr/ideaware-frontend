const formatDate = (date: Date) => {
	const month = date.getDate();
	const year = date.getFullYear();
	const day = date.getDay();
	const hour = date.getHours();
	const minute = date.getMinutes();

	return `${day}/${month}/${year} ${hour}:${minute}`;
};

export default formatDate;
