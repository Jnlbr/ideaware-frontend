import axios from 'axios';

const { error, log } = console;

const instance = axios.create({
	baseURL: process.env.API_URL,
	timeout: 15000
	// responseType: 'json'
});

const mapError = (error) => error.response.data;
const mapSuccess = (response) => ({
	...response.data,
	method: response.config.method
});

export const handleResponseSuccess = (response) => {
	log(`${response.status}: ${response.statusText}: 
${response.config.url}`);
	// console.log(response);
	return mapSuccess(response);
};

export const handleResponseError = (_error) => {
	if (_error.response) {
		error(`${_error.response.status}: ${_error.response.statusText}
${_error.response.data.message || 'Unknown error'}`);
	}

	return Promise.reject(_error);
};

export const handleRequestConfig = (config) => {
	return config;
};

export const handleRequestError = (_error) => {
	error(`Request error:
${JSON.stringify(_error.request)}`);
	return Promise.reject(_error);
};

// Request interceptor
instance.interceptors.request.use(handleRequestConfig, handleRequestError);
// Response interceptor
instance.interceptors.response.use(handleResponseSuccess, handleResponseError);

export default instance;
