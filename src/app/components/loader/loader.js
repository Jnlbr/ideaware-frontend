// @flow
import React from 'react';
import classes from './loader.module.scss';

type LoaderProps = {
	size: number
};

const Loader = ({ size }: LoaderProps) => (
	<div className={classes.idsRing} style={{ width: size, height: size }}>
		<div
			style={{
				width: size,
				height: size
			}}
		/>
		<div
			style={{
				width: size,
				height: size
			}}
		/>
		<div
			style={{
				width: size,
				height: size
			}}
		/>
		<div
			style={{
				width: size,
				height: size
			}}
		/>
	</div>
);

Loader.defaultProps = {
	size: 24
};

export default Loader;
