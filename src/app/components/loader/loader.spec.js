import React from 'react';
import { shallow } from 'enzyme';
import Loader from './loader';

describe('Loader props', () => {
	const props = {
		size: 18
	};
	const container = shallow(<Loader size={props.size} />);

	test('Should match the snapshot', () => {
		expect(container.html()).toMatchSnapshot();
	});
	test('Should have the assigned props', () => {
		expect(
			container
				.find('div')
				.at(0)
				.prop('style')
		).toEqual({ width: props.size, height: props.size });
	});
});
