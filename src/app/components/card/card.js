// @flow
import React from 'react';
import type { Node } from 'react';
import classes from './card.module.scss';
import classnames from 'classnames';

type CardProps = {
	children: Node,
	className: string,
	style: Object,

	// Props
	containerProps: Object
};

const Card = ({ children, className, style, containerProps }: CardProps) => (
	<div
		className={classnames(classes.container, className)}
		style={style}
		{...containerProps}
	>
		{children}
	</div>
);

export default Card;
