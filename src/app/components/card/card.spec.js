import React from 'react';
import { shallow } from 'enzyme';
import Card from './card';

describe('Card children', () => {
	const container = shallow(
		<Card>
			<p>t1</p>
			<p>t2</p>
		</Card>
	);
	test('Should match the snapshot', () => {
		expect(container.html()).toMatchSnapshot();
	});
	test('Should has 2 <p /> children', () => {
		expect(container.find('p').length).toEqual(2);
	});
});
