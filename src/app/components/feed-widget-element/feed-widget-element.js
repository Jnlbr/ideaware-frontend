// @flow
import React from 'react';
import classes from './feed-widget-element.module.scss';
// Components
import Card from '../card';
import formatDate from '../../utils/format-date';

type FeedWidgetElementProps = {
	date: Date,
	author: string,
	message: string
};

const FeedWidgetElement = ({ date, author, message }: FeedWidgetElementProps) => {
	if (!date instanceof Date) {
		date = new Date(date);
	}

	return (
		<Card className={classes.container}>
			<div className={classes.header}>
				<p>{author}</p>
				<p>{formatDate(date)}</p>
			</div>
			<p>{message}</p>
		</Card>
	);
};

export default FeedWidgetElement;
