import React from 'react';
import { shallow } from 'enzyme';
import FeedWidgetElement from './feed-widget-element';
import formatDate from '../../utils/format-date';

describe('FeedWidgetElement props', () => {
	const props = {
		date: new Date('1999-12-23'),
		author: 'Rick Riordan',
		message: 'Greek gods exist'
	};

	const container = shallow(
		<FeedWidgetElement
			date={props.date}
			author={props.author}
			message={props.message}
		/>
	);

	test('Should match the snapshot', () => {
		expect(container.html()).toMatchSnapshot();
	});

	test('Should have the assigned props', () => {
		expect(
			container
				.find('p')
				.at(0)
				.text()
		).toEqual(props.author);
		expect(
			container
				.find('p')
				.at(1)
				.text()
		).toEqual(formatDate(props.date));
		expect(
			container
				.find('p')
				.at(2)
				.text()
		).toEqual(props.message);
	});
});
