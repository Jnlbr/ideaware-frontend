// @flow
import React, { useEffect, useState } from 'react';
import classes from './feed-widget.module.scss';
// Modules
import classnames from 'classnames';
// Components
import FeedWidgetElement from '../feed-widget-element';
import Loader from '../loader';
import Card from '../card';
// Hooks
import useHttp from '../../hooks/use-http';
import useInterval from '../../hooks/use-interval';
import InputLabel from '../input-label/input-label';

export type FeedWidgetProps = {
	// Data
	url: string,
	limit: number,
	updateInterval: number,
	title: string,
	editable: boolean,
	// Styles
	className: string
};

const FeedWidget = ({
	// Data
	url,
	limit: _limit,
	updateInterval: _updateInterval,
	title,
	editable,
	// Styles
	className
}: FeedWidgetProps) => {
	const [data, setData] = useState([]);
	const [limit, setLimit] = useState(_limit);
	const [updateInterval, setUpdateInterval] = useState(_updateInterval);
	const [loading, response, error, fetchApi] = useHttp(
		`${url}?limit=${limit}`,
		[],
		true
	);
	const timeLeft = useInterval(fetchApi, updateInterval);

	useEffect(() => {
		const values = Object.values(response);
		// The last element isn't a post
		values.pop();
		setData(values);
	}, [response]);

	return (
		<Card className={classnames(classes.container, className)}>
			<div className={classes.header}>
				<h3>{title}</h3>
				<div className={classes.headerRight}>
					{loading || timeLeft === 0 ? <Loader /> : <h3>{timeLeft}</h3>}
				</div>
			</div>
			{editable && (
				<>
					<InputLabel
						className={classes.inputLabel}
						label="Limit"
						value={limit}
						type="number"
						onChange={setLimit}
					/>
					<InputLabel
						className={classes.inputLabel}
						label="Interval"
						value={updateInterval}
						type="number"
						onChange={setUpdateInterval}
					/>
					<small>* Changes will be made after the next refresh</small>
				</>
			)}
			{data.map((post, i) => (
				<FeedWidgetElement
					key={`${post.id}-${i}`}
					date={post.created_at}
					author={post.user.name}
					message={post.text}
				/>
			))}
		</Card>
	);
};

export default FeedWidget;
