import React from 'react';
import { shallow } from 'enzyme';
import FeedWidget from './feed-widget';
import FeedWidgetElement from '../feed-widget-element';
import InputLabel from '../input-label';

describe('FeedWidget state', () => {
	const props = {
		limit: 5,
		updateInterval: 10,
		url: 'http://api.massrelevance.com/MassRelDemo/kindle.json',
		title: 'Test title'
	};
	const container = shallow(
		<FeedWidget
			limit={props.limit}
			updateInterval={props.updateInterval}
			url={props.url}
			title={props.title}
			editable
		/>
	);

	test('Should match the snapshot', () => {
		expect(container.html()).toMatchSnapshot();
	});

	test('Should have limit and updateInterval state', () => {
		expect(
			container
				.find(InputLabel)
				.at(0)
				.props().value
		).toEqual(props.limit);
		expect(
			container
				.find(InputLabel)
				.at(1)
				.props().value
		).toEqual(props.updateInterval);
	});
});
