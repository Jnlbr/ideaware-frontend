import React from 'react';
import { shallow } from 'enzyme';
import InputLabel from './input-label';

describe('InputLabel props', () => {
	let value = 0;
	const onChange = (_value) => (value = _value);

	const type = 'number',
		label = 'Test label',
		placeholder = 'Test placeholder';

	const container = shallow(
		<InputLabel
			type={type}
			label={label}
			placeholder={placeholder}
			value={value}
			onChange={onChange}
		/>
	);

	test('Should match the snapshot', () => {
		expect(container.html()).toMatchSnapshot();
	});
	test('Should have the assigned props', () => {
		expect(container.find('input').props()).toEqual({
			onChange: expect.any(Function),
			placeholder,
			type,
			value
		});
		expect(container.find('label').text()).toEqual(`${label}:`);
	});
	test('Should update the local value to the new value', () => {
		const newValue = 10;
		container.find('input[type="number"]').simulate('change', {
			target: {
				value: newValue
			}
		});
		expect(value).toEqual(newValue);
	});
});
