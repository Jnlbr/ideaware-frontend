// @flow
import React from 'react';
import classes from './input-label.module.scss';
// Modules
import classnames from 'classnames';
// Utils
import { isNumber } from '../../utils/validations';

type InputLabelProps = {
	// Styles
	className: string,
	// Data
	label: string,
	value: string,
	type: string,
	placeholder: string,
	// Methods
	onChange: () => void
};

const InputLabel = ({
	// Styles
	className,
	// Data
	label,
	value,
	type,
	placeholder,
	// Methods
	onChange
}: InputLabelProps) => {
	const handleChange = (evt) => {
		const value = evt.target.value;
		// Firefox returns an emtpy string
		if (value !== '') {
			onChange(value);
		}
	};

	return (
		<div className={classnames(classes.container, className)}>
			<label>{label}:</label>
			<input
				value={value}
				type={type}
				placeholder={placeholder}
				onChange={handleChange}
			/>
		</div>
	);
};

InputLabel.defaultProps = {
	type: 'text',
	placeholder: ''
};

export default InputLabel;
