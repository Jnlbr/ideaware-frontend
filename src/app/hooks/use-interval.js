import { useState, useEffect } from 'react';

const SECOND = 1000;

function useInterval(cb: () => void, delay: number) {
	const [current, setCurrent] = useState(delay);

	useEffect(() => {
		const tick = () => {
			if (!current) {
				setCurrent(delay);
				cb();
			} else {
				setCurrent(current - 1);
			}
		};
		const interval = setTimeout(tick, SECOND);

		return () => clearTimeout(interval);
	}, [current]);

	return current;
}

export default useInterval;
