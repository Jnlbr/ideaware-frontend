import { useState, useEffect } from 'react';
import axios from '../utils/axios';

function useHttp(url: string, defaultValue: any, atMount: string = false) {
	const [loading, setLoading] = useState(false);
	const [error, setError] = useState(null);
	const [response, setResponse] = useState(defaultValue);

	const fetchApi = async () => {
		setLoading(true);
		try {
			const response = await axios.get(url);
			setResponse(response);
		} catch (err) {
			setError(error);
		} finally {
			setLoading(false);
		}
	};

	useEffect(() => {
		if (atMount) {
			fetchApi();
		}
	}, []);

	return [loading, response, error, fetchApi];
}

export default useHttp;
