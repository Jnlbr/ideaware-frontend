// @flow
import React from 'react';
import classes from './home.module.scss';
import { Link } from 'react-router-dom';
import FeedWidget from '../../components/feed-widget/feed-widget';

type HomeProps = {};

const Home = (props: HomeProps) => {
	return (
		<div className={classes.container}>
			<FeedWidget
				title="Left feed"
				url="http://api.massrelevance.com/MassRelDemo/kindle.json"
				limit={10}
				updateInterval={3}
				className={classes.widgetLeft}
				editable
			/>
			<FeedWidget
				title="Right feed"
				className={classes.widgetRight}
				url="http://api.massrelevance.com/MassRelDemo/kindle.json"
				limit={10}
				updateInterval={10}
			/>
		</div>
	);
};

export default Home;
