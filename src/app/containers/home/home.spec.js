import React from 'react';
import { shallow } from 'enzyme';
import Home from './home';

describe('<Home />', () => {
	const container = shallow(<Home />);

	test('Should match the snapshot', () => {
		expect(container.html()).toMatchSnapshot();
	});
});
